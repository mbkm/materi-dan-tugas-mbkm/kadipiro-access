const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const path = require("path");


const config = require(`./config/app.config.json`);
const mysql = new (require(`./class/mariadb.class.js`))(config.db);


app.get('/', (req, res)=>{
    res.sendFile(path.join(__dirname, 'public/index.html'));
});


async function mhsExist(){
    return await mysql
      .query(
        `SELECT 
            aa.pesertaId,
            bb.name,
            aa.timestampMasuk
        FROM s_log aa
        LEFT JOIN ref_peserta bb ON aa.pesertaId = bb.id
        WHERE DATE(timestampMasuk) = DATE(NOW()) 
        AND timestampKeluar IS NULL
        ORDER BY aa.timestampMasuk`
      )
      .catch((err) => []);
}



io.on("connection", async (socket) => {
    const mhs = await mysql.query(
        `SELECT id, name FROM ref_peserta ORDER BY name ASC`
    ).catch(err => [])

    socket.emit("info/listMhs", JSON.stringify(mhs));

    const currentMhs = await mhsExist();
    socket.emit("info/currentMhs", JSON.stringify(currentMhs));

    socket.on("masuk", async (data) => {
        try{
            data = JSON.parse(data);

            if(!data.id || data.id == "Choose..."){
                data = null
            }
        }catch(err){
            data = null
        }

        if(!data){
            socket.emit("info/alert", JSON.stringify({
                isSuccess : false,
                message : "Format data salah / kosong"
            }))
            return
        }

        const isExist = await mysql
          .query(
            `SELECT timestampMasuk 
            FROM s_log 
            WHERE pesertaId = ?
            AND DATE(timestampMasuk) = DATE(NOW())
            AND timestampKeluar IS NULL`,
            [data.id]
          )
          .then((res) => true)
          .catch((err) => false);

        if(isExist){
            socket.emit("info/alert", JSON.stringify({
                isSuccess : false,
                message : "Sudah ngisi masuk"
            }))
            return
        }

        await mysql.query(
            `INSERT INTO s_log(
                pesertaId
            ) VALUES (
                ?
            )`,
            [data.id]
        )

        socket.emit("info/alert", JSON.stringify({
            isSuccess : true,
            message : "Sip ! Cakep"
        }))

        const currentMhs = await mhsExist();
        io.emit("info/currentMhs", JSON.stringify(currentMhs));
        return
    });

    socket.on("keluar", async (data) => {
        
        try{
            data = JSON.parse(data);

            if(!data.id || data.id == "Choose..."){
                data = null
            }
        }catch(err){
            data = null
        }

        if(!data){
            socket.emit("info/alert", JSON.stringify({
                isSuccess : false,
                message : "Format data salah / kosong"
            }))
            return
        }

        const isExist = await mysql
          .query(
            `SELECT timestampMasuk 
            FROM s_log 
            WHERE pesertaId = ?
            AND DATE(timestampMasuk) = DATE(NOW())`,
            [data.id]
          )
          .then((res) => true)
          .catch((err) => false);

        if(!isExist){
            socket.emit("info/alert", JSON.stringify({
                isSuccess : false,
                message : "Masuk dulu dong, baru keluar"
            }))
            return
        }


        await mysql.query(
          `UPDATE s_log
            SET timestampKeluar = NOW()
            WHERE pesertaId = ?
            AND DATE(timestampMasuk) = DATE(NOW())
            AND timestampKeluar IS NULL`,
          [data.id]
        );

        socket.emit("info/alert", JSON.stringify({
            isSuccess : true,
            message : "Sip ! Cakep"
        }))

        const currentMhs = await mhsExist();
        io.emit("info/currentMhs", JSON.stringify(currentMhs));
        return;
    });
})

server.listen(config.port);
    
